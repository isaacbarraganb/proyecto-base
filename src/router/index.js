import Vue from 'vue';
import VueRouter from 'vue-router';
const Home = () => import('../views/Home.vue')
const Auth = () => import('../views/Auth.vue');
const Profile = () => import('../views/Profile.vue');
import store from '../store'
Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta:{
            requiresAuth: true
        }
    },
    {
        path: '/auth',
        name: 'auth',
        component: Auth
    },
    {
        path: '/profile',
        name: 'profile',
        component: Profile,
        meta:{
            requiresAuth: true
        }
    }
]

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});


router.beforeEach(async(to, from, next)=>{
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    console.log('ALcance aqui')
    if(requiresAuth && !(await store.dispatch('user/getCurrentUser'))){
        console.log('Estoy en el if');
        next({name: 'auth'});
    }else if(!requiresAuth && (await store.dispatch('user/getCurrentUser'))){
        console.log('Estoy en el else if');
        next({name: 'home'});
    }else{
        console.log('Estoy en el else');
        next();
        
    }
})

export default router;