
const state = {
    user: null,
    dataFriends: [
        { id: 1, name: "Isaac", correo: "isaac@hotmail.com"},
            { id: 2, name: "Raulito", correo: "chango@hotmail.com"},
            { id: 3, name: "Edgar", correo: "edgar@hotmail.com"},
            { id: 4, name: "Martin", correo: "martin@hotmail.com"},
            { id: 5, name: "Jhon", correo: "jhon@hotmail.com"}
    ],
}

const getters = {
    getNameUserToChat(state, id){
        return state.dataFriends.find(dataFriends => {
            return 'Soy el dato que necesitan : '+dataFriends.id == id
        })
    }
}
const mutations = {
    setUser(state, user){
        state.user = user;
    }
}

const actions = {
    getCurrentUser(){
        return true;
    },
    doRegisterUser({commit}, {user,email,pass}){
        console.log(user);
        console.log(email);
        console.log(pass);

        let dataUser = [];
        dataUser.user = user;
        dataUser.email = email;
        dataUser.password = pass;
        commit('setUser', dataUser);
    },
    
}

export default{
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}