const state = {
    chats:[]
}

const mutations = {
    setChats(state, chatUser){
        state.chats.push({
            id: chatUser.id,
            name: chatUser.name,
            open: true,
        });
    },
    decrementChat(state, id){
        state.chats.splice(id, 1);
    },
    chatDown(state, index){
        state.chats[index].open = false;
    },
    chatUp(state, index){
        state.chats[index].open = true;
    }
    
}

const actions = {
    async openChat(context, chatUser){

        //BUsca en el arreglo de chats si el id que se esta introduciendo ya existe para si el usuari
        //lo presiona nuevamente este ya no se agregue al arreglo y ocacione un error
        let idUser = context.state.chats.find(idUser => idUser.id === chatUser.id);
        
        if(idUser){
            
            console.log('Esta ventana se encuentra abierta');
        }else{
            
            context.commit('setChats', chatUser);
            console.log('Chat : '+context.state.chats[0].open);
        }
        
    },
    async closeChat(context, id){

        //console.log('El ide que eliminare sera '+ context.state.chats.length +" soy separador "+ id);
        context.commit('decrementChat', id);
    },
    async downChat(context, index){
        //console.log(index)
        context.commit('chatDown', index);
        //console.log('Minimizaa : '+ context.state.chats[index].open)
    },
    async upChat(context, index){
        context.commit('chatUp', index);
    }
}


export default{
    namespaced: true,
    state,
    mutations,
    actions
}