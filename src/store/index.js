import Vue from 'vue';
import Vuex from 'vuex';
import user from './user.js';
import chats from './chats.js'
Vue.use(Vuex);

const store = new Vuex.Store({
    state:{},
    mutations:{},
    actions:{
        
    },
    getters:{

    },
    modules:{
        user,
        chats
    }
});

export default store;